FROM python:3-alpine

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]
# hadolint ignore=DL3013,DL3018
RUN \
    apk upgrade &&\
    apk add --no-cache --virtual builddeps \
        gcc musl-dev \
    && pip --disable-pip-version-check install --no-cache-dir \
        safety \
        yamllint \
    && safety check \
    && pip uninstall -y safety \
    && apk del builddeps \
    && rm -rf /root/.cache
RUN wget -q -O - https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /opt/bin/trivy \
    && export PATH="$PATH":/opt/bin/trivy \
    && trivy filesystem --skip-dirs /opt/bin/trivy --exit-code 1 --no-progress / \
    && trivy image --reset \
    && rm -rf /opt/bin/trivy \
    && rm -rf /root/.cache
WORKDIR /yaml
ENTRYPOINT [ "yamllint", "-f", "colored" ]
