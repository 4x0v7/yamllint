# Dockerised yamllint

> A simple alpine-based Python with `yamllint` installed

## How to use

```shell
docker pull registry.gitlab.com/4x0v7/yamllint
```

To lint a yaml file in your current working directory, mount it into `/yaml`

This will also use a `.yamllint` configuration file if you have one

```shell
docker run -v ${PWD}:/yaml 4x0v7/yamllint yamlfile.yml
```
